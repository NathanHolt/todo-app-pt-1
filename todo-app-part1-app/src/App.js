import React, { useState } from "react";
import { todos as todosList } from "./todos";
import { v4 as uuid } from "uuid";

function App() {
  const [todos, setTodos] = useState(todosList);
  const [inputText, setInputText] = useState("");

  const handleAddToDo = (event) => {
    // console.log(event.which);
    if (event.which === 13) {
      const newID = uuid();

      const newToDo = {
        userId: 1,
        id: newID,
        title: inputText,
        completed: false,
      };

      const newToDos = { ...todos };
      newToDos[newID] = newToDo;
      setTodos(newToDos);

      setInputText("");
    }
  };

  const handleToggle = (id) => {
    const newToDos = { ...todos };
    newToDos[id].completed = !newToDos[id].completed;
    setTodos(newToDos);
  };

  const handleDeleteToDo = (id) => {
    const newToDos = { ...todos };
    delete newToDos[id];
    setTodos(newToDos);
  };

  const handleClearCompleted = () => {
    const newToDos = { ...todos };
    for (const todo in newToDos) {
      if (newToDos[todo].completed) {
        delete newToDos[todo];
      }
    }
    setTodos(newToDos);
  };

  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <input
          onChange={(event) => setInputText(event.target.value)}
          onKeyDown={(event) => handleAddToDo(event)}
          className="new-todo"
          placeholder="What needs to be done?"
          value={inputText}
          autoFocus
        />
      </header>
      <TodoList todos={Object.values(todos)} handleToggle={handleToggle} handleDeleteToDo={handleDeleteToDo} />
      <footer className="footer">
        <span className="todo-count">
          <strong>0</strong> item(s) left
        </span>
        <button onClick={() => handleClearCompleted()} className="clear-completed">
          Clear completed
        </button>
      </footer>
    </section>
  );
}

function TodoItem(props) {
  return (
    <li className={props.completed ? "completed" : ""}>
      <div className="view">
        <input
          className="toggle"
          type="checkbox"
          checked={props.completed}
          onChange={() => props.handleToggle(props.id)}
        />
        <label>{props.title}</label>
        <button onClick={() => props.handleDeleteToDo(props.id)} className="destroy" />
      </div>
    </li>
  );
}

function TodoList(props) {
  return (
    <section className="main">
      <ul className="todo-list">
        {props.todos.map((todo) => (
          <TodoItem
            title={todo.title}
            completed={todo.completed}
            id={todo.id}
            handleToggle={props.handleToggle}
            handleDeleteToDo={props.handleDeleteToDo}
            key={todo.id}
          />
        ))}
      </ul>
    </section>
  );
}

export default App;
